FROM python:3

# Create app folder
RUN mkdir -p /usr/src/app

# Set PWD to the app folder
WORKDIR /usr/src/app

# Bundle app source
COPY /server.py /usr/src/app/server.py
COPY /requirements.txt /usr/src/app/requirements.txt

# Install dependencies
RUN pip install -r requirements.txt

# Expose the Flask port
#EXPOSE 5000

CMD [ "python", "server.py" ]