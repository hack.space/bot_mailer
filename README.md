# Bot Mailer

## Build docker container

- `docker build -t bot_mailer .`
- `docker run --name bot_mailer -p 5000:5000 --rm -d -e TOKEN={TOKEN}-e HOST="0.0.0.0"  bot_mailer`

## Env variables

- `TOKEN` (required) - bot token
- `HOST` (optional, default: localhost) - host where server will be running
- `PORT` (optional, default: 5000) - port where server will running


## API methods

- Send list of plan messages

```
POST /:token/send_messages
Body: [{
  user_tg_id: string, 
  text: number
}]
Response:
  200: [{
    tgId: string ("success" | "failed")
  }]
```

- Send list of messages with Inline buttons: "Yes", "No", "Maybe"
```
POST /:token/send_verify_requests
Body: [{
  user_tg_id: string, 
  text: number
}]
Response:
  200: [{
    tgId: string ("success" | "failed")
  }]
```